# frozen_string_literal: true

Rails.application.routes.draw do
  resources :oeuvres
  root to: 'oeuvres#index'
end
