class OeuvresController < ApplicationController
  def index
    @oeuvres = Oeuvre.all
  end

  def new
    @oeuvre = Oeuvre.new
  end

  def create
    oeuvre = Oeuvre.new(oeuvre_params)
    oeuvre.save
    redirect_to oeuvres_path
  end

  private
  def oeuvre_params
    params.require(:oeuvre).permit(:nom, :artiste, :description, :residence_annee, :photo)
  end
end
