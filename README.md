# Empreintes

Emprunter les oeuvres des artistes de Châteldon

# Installation

## Création des rôles de la base de données

Les informations nécessaire à l'initialisation de la base doivent être pré-configurées à la main grâce à la procédure suivante :

    su - postgres
    psql
    > create user empreintes_dev with password 'empreintes_dev' superuser;
    > create user empreintes_test with password 'empreintes_test' superuser;
    > \q
