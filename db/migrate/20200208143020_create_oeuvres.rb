class CreateOeuvres < ActiveRecord::Migration[6.0]
  def change
    create_table :oeuvres do |t|
      t.string :nom
      t.string :artiste
      t.string :description
      t.string :artiste_description
      t.integer :residence_annee

      t.timestamps
    end
  end
end
